// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "ProjectCrystalGameMode.generated.h"

UCLASS(minimalapi)
class AProjectCrystalGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AProjectCrystalGameMode();
};



