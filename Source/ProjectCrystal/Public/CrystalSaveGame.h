// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/SaveGame.h"
#include "Public/BaseCharacter.h"
#include "CrystalSaveGame.generated.h"

//https://docs.unrealengine.com/latest/INT/Gameplay/SaveGame/Code/index.html

//Used in widgets to determine what mode to display load/save widgets in.
UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class ELoadSaveEnum : uint8
{
	LS_Save 	UMETA(DisplayName = "Save"),
	LS_Load 	UMETA(DisplayName = "Load"),
	LS_Delete	UMETA(DisplayName = "Delete")
};

/**
 * 
 */
UCLASS()
class PROJECTCRYSTAL_API UCrystalSaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Basic)
	FString saveSlotName;

	UPROPERTY(VisibleAnywhere, Category = Basic)
	uint32 saveSlotIndex;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Basic)
	FVector playerSpawnLocation;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Basic)
	FVector saveLocation;

	//This is the actual file name of the map
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Basic)
	FName mapFileName;

	//This is the name of the map to display in game
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Basic)
	FName mapDisplayName;

	//todo: add list of active party members in the party.  

	//Change this to a list of charStats based off of the party members
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Basic)
		FCharacterStats charStats;

	UCrystalSaveGame();
	
};
