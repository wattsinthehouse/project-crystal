// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "CrystalAIController.generated.h"

//Used in widgets to determine what mode to display load/save widgets in.
//UENUM(BlueprintType)		//"BlueprintType" is essential to include
//enum class EAIBlackboardMode : uint8
//{
//	BBMode_Patrol 		UMETA(DisplayName = "Patrol"),
//	BBMode_Battle 		UMETA(DisplayName = "Battle"),
//	BBMode_ChaseActor	UMETA(DisplayName = "ChaseActor"),
//	BBMode_BossBattle	UMETA(DisplayName = "BossBattle")
//};

/**
 * 
 */
UCLASS()
class PROJECTCRYSTAL_API ACrystalAIController : public AAIController
{
	GENERATED_BODY()
	
public:

	virtual void Possess(class APawn* InPawn) override;

	void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "Patrol")
	AActor* FindPlayer();

	void PlayerLost();
	
	//Array of actors that can be spotted with line of sight to to start battle
	UPROPERTY()
	TArray<AActor*> actorsInPatrolArea;
	
};
