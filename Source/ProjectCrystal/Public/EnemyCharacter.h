// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseCharacter.h"
#include "Components/BoxComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Public/AI_WaypointActor.h"
#include "Public/BaseWeapon.h"
#include "EnemyCharacter.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTCRYSTAL_API AEnemyCharacter : public ABaseCharacter
{
	GENERATED_BODY()

public:
	AEnemyCharacter(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;

	//This component is used to start the battle when the player comes in range of the enemy
	UPROPERTY(VisibleAnywhere, Category = "Misc")
	USphereComponent* battleSphereComponent;

	//This component is used to start determining when the Enemy should start searching for the player (once they are in range to be found by enemy)
	UPROPERTY(VisibleAnywhere, Category = "Misc")
	UBoxComponent* patrolBoxComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Battle")
	int32 goldDropAmount;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character")
	UBehaviorTree* behaviorTree;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character")
	TArray<AAI_WaypointActor*> waypointNodes;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character")
	TSubclassOf<ABaseEquipment> weaponToSpawn;

private:
	UFUNCTION()
	void OnWorldOverlapBegin(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	void OnWorldOverlapEnd(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void OnPatrolBoxOverlapBegin(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	void OnPatrolBoxOverlapEnd(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser);
	
};
