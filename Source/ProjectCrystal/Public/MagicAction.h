// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseAction.h"
#include "MagicAction.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTCRYSTAL_API AMagicAction : public ABaseAction
{
	GENERATED_BODY()

public:

	virtual void ActivateAction(AActor* actorInstigator, AActor* actorTarget) override;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Magic")
	float mpCost;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Rychly")
	float fastMaxWalkSpeed;

private:
	void ActivateFastAction(AActor* actorTarget);
};
