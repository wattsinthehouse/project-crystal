// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Public/BaseAction.h"
#include "Animation/AnimMontage.h"
#include "Public/BaseEquipment.h"
#include "Public/BaseInteractObject.h"
#include "BaseCharacter.generated.h"

USTRUCT()
struct FCharacterStats {

	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly, Category = "Stats")
	float currentHealth;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stats")
	float maxHealth;

	UPROPERTY(BlueprintReadOnly, Category = "Stats")
	float currentMagic;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stats")
	float maxMagic;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stats")
	float strengthStat;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stats")
	float magicStat;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stats")
	float vitalityStat;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stats")
	float defenseStat;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stats")
	float characterLevel;
};

UCLASS()
class PROJECTCRYSTAL_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser);
	void StartDeathProcess();
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Action")
	TArray<TSubclassOf<ABaseAction>> equippedActions;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Battle")
	UParticleSystem* lockOnTargetParticleSystem;

	//void PlayAnimation(ABaseAction* action);
	UPROPERTY()
	TArray<AActor*> actorsBeingHit;

	UPROPERTY()
	ABaseAction* activatedAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stats")
	FCharacterStats charStats;

	UPROPERTY(BlueprintReadOnly, Category = "Battle")
	bool bIsDead;

	//Call this function to be customized in blueprints when actor dies
	UFUNCTION(BlueprintImplementableEvent, Category = "World")
	virtual void CPPRequest_OnDeath();

	UPROPERTY(BlueprintReadOnly, Category = "Battle")
	bool bInBattle;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Equipment")
	ABaseEquipment* equippedWeapon;

	UFUNCTION(BlueprintImplementableEvent, Category = "World")
	virtual void CPPRequest_StartInteractProcess();

	UFUNCTION(BlueprintImplementableEvent, Category = "World")
	virtual void CPPRequest_StopInteractProcess();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Interact")
	UAnimMontage* interactObjectPullMontage;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Interact")
	ABaseInteractObject* interactObject;

	void SpawnWeapon(UClass* weapon);

	//this function will house any data cleaning that is needed on death (ie, remove equipped weapon, stats, etc.)
	void CleanUpOnDeath();
};
