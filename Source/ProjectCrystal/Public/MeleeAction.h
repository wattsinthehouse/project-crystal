// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseAction.h"
#include "MeleeAction.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTCRYSTAL_API AMeleeAction : public ABaseAction
{
	GENERATED_BODY()
	
	virtual void ActivateAction(AActor* actorInstigator, AActor* actorTarget);
	
private:
	void ActiveLungeAction(AActor* actorInstigator, AActor* actorTarget);
	
};
