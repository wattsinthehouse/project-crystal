// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "Public/EnemyCharacter.h"
#include "CrystalPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTCRYSTAL_API ACrystalPlayerController : public APlayerController
{
	GENERATED_BODY()
	

public:
	ACrystalPlayerController();
	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

	void MoveForward(float value);
	void MoveRight(float value);
	void MoveCharacter(const FVector direction, float value);
	void TurnAtRate(float Rate);
	void LookUpAtRate(float Rate);
	
	//Action calls
	void OnActionOne();
	void OnActionOneRelease();
	void OnActionTwo();
	void OnActionTwoRelease();
	void OnActionThree();
	void OnActionThreeRelease();
	void OnActionFour();
	void OnActionFourRelease();
	void OnActionInteract();
	void OnActionInteractRelease();
	void OnActionOpenPartyMenu();
	void OnActionOpenPartyMenuRelease();

	void ActionLockOnProcess();
	void CameraFollowLockOnTarget();
	void ClearActivatedAction();

	bool bHasTarget;
	AEnemyCharacter* enemyTarget;

	void ScanAndSetEnemyTarget();

private:

	
};
