// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "BaseItem.generated.h"

UENUM(BlueprintType)
enum class EItemType : uint8
{
	IT_Sword UMETA(DisplayName = "Equipment_Sword"),
	IT_Gun UMETA(DisplayName = "Equipment_Gun")
};

USTRUCT()
struct FItemStats {

	GENERATED_USTRUCT_BODY()

	//This variable is the name that is displayed in the party menu for equipping/loot/selling/etc.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
	FName inGameItemName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
	UTexture2D* inventoryIcon;

	//Enum used for logic in C++ and Blueprints.  Indicates what type of item it is
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
	EItemType itemType;

};

UCLASS()
class PROJECTCRYSTAL_API ABaseItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseItem();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	bool IsValidRootComponent();

	UPROPERTY(Category = Item, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UCapsuleComponent* CapsuleComponent;
	
	UPROPERTY(Category = Item, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
	FItemStats itemStats;
};
