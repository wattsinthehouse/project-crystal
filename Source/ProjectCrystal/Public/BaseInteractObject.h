// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "BaseInteractObject.generated.h"

UCLASS()
class PROJECTCRYSTAL_API ABaseInteractObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseInteractObject(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(VisibleAnywhere, Category = "Switch Components")
	USphereComponent* overlaySphereComponent;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	//virtual void Interact(AActor* instigator);
	virtual void StartInteract();
	virtual void FireInteractEvent();
	virtual void StopInteract();

	UFUNCTION()
	virtual void OnWorldOverlapBegin(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	virtual void OnWorldOverlapEnd(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintImplementableEvent, Category = "World")
	virtual void CPPRequest_OnOverlapBegin();
	UFUNCTION(BlueprintImplementableEvent, Category = "World")
	virtual void CPPRequest_OnOverlapEnd();
	UFUNCTION(BlueprintImplementableEvent, Category = "World")
	virtual void CPPRequest_CompleteInteract();

	UPROPERTY()
	AActor* instigatorActor;

	//If set to true, then this object can be interacted with
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Interact")
	bool bInteractable;
	
};
