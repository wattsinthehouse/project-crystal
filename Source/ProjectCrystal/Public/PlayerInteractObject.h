// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseInteractObject.h"
#include "PlayerInteractObject.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTCRYSTAL_API APlayerInteractObject : public ABaseInteractObject
{
	GENERATED_BODY()
	
public:
	APlayerInteractObject(const FObjectInitializer& ObjectInitializer);

	virtual void StartInteract() override;
	UFUNCTION(BlueprintCallable, Category = "Interact")
	virtual void FireInteractEvent() override;
	virtual void StopInteract() override;

	UPROPERTY(Category = Interact, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* mesh;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Interact")
	UAnimMontage* montageOnInteract;

	UFUNCTION()
	virtual void OnWorldOverlapBegin(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) override;

	UFUNCTION()
	virtual void OnWorldOverlapEnd(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;
	
};
