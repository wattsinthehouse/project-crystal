// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "AI_WaypointActor.generated.h"

UCLASS()
class PROJECTCRYSTAL_API AAI_WaypointActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAI_WaypointActor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	//This is the number that will be "next" in the patrol for the AI
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "World")
	int32 positionInPatrol;

	
	
};
