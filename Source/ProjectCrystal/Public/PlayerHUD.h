// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "PlayerHUD.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTCRYSTAL_API APlayerHUD : public AHUD
{
	GENERATED_BODY()

public:
	// Call to BP function.
	UFUNCTION(BlueprintImplementableEvent, Category = "Battle")
	void CPPRequest_DrawStartBattleAnimation();
	
};
