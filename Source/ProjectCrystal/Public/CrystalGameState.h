// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameState.h"
#include "Public/PlayerCharacter.h"
#include "Public/EnemyCharacter.h"
#include "Public/CrystalSaveGame.h"
#include "Kismet/GameplayStatics.h"
#include "CrystalGameState.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTCRYSTAL_API ACrystalGameState : public AGameState
{
	GENERATED_BODY()
	
public:
	ACrystalGameState();

	//This is the main player in the game (Player 1) that will be used to drive certain events.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player")
	APlayerCharacter* player;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player")
	TArray<TSubclassOf<APlayerCharacter>> activePartyMembers;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player")
	TArray<TSubclassOf<APlayerCharacter>> nonActivePartyMembers;

	//Used to save the variables to the index
	UFUNCTION(BlueprintCallable, Category = "SaveLoad")
	void SaveGame(ABaseCharacter* playerCharacter, int32 saveSlotIndex);

	//Returns the save information at this index.  Used to display data on the save/load screens.
	UFUNCTION(BlueprintCallable, Category = "SaveLoad")
	UCrystalSaveGame* GetSaveGameData(int32 saveSlotIndex);

	//Used to load the level and set the variables at the given index
	UFUNCTION(BlueprintCallable, Category = "SaveLoad")
	void LoadSaveGame(int32 saveSlotIndex);

	UFUNCTION(BlueprintCallable, Category = "SaveLoad")
	void DeleteSaveGame(int32 saveSlotIndex);

	UFUNCTION(BlueprintCallable, Category = "Battle")
	void RemoveBattleEnemy(AEnemyCharacter* deadEnemy);

	UFUNCTION(BlueprintCallable, Category = "Battle")
	void CheckEndBattleStatus();

	UFUNCTION(BlueprintCallable, Category = "Battle")
	void ResetBattleVariables();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Battle")
	bool bInBattle;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Battle")
	int32 goldFromBattle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "World")
	FName mapFileName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "World")
	FName mapDisplayName;

	UFUNCTION(BlueprintCallable, Category = "World")
	void SpawnActivePartyMembers();

	UFUNCTION(BlueprintCallable, Category = "World")
	void AddActivePartyMember(APlayerCharacter* playerToAdd);

	UFUNCTION(BlueprintCallable, Category = "World")
	void RemoveActivePartyMember(APlayerCharacter* playerToRemove);
	
	UFUNCTION(BlueprintCallable, Category = "World")
	void AddNonActivePartyMember(APlayerCharacter* playerToAdd);

	UFUNCTION(BlueprintCallable, Category = "World")
	void RemoveNonActivePartyMember(APlayerCharacter* playerToRemove);
};
