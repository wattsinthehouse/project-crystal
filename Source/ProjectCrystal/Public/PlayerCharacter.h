// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseCharacter.h"
#include "Public/InventoryManager.h"
#include "Public/EnemyCharacter.h"
#include "PlayerCharacter.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTCRYSTAL_API APlayerCharacter : public ABaseCharacter
{
	GENERATED_BODY()


	
public:
	APlayerCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void StartBattle();

	UFUNCTION(BlueprintCallable, Category = "World")
	void EndBattle();
	
	UPROPERTY(BlueprintReadOnly, Category="Inventory")
	UInventoryManager* inventoryManager;
	//UParticleSystemComponent* focusParticalSystem

	UPROPERTY()
	UParticleSystemComponent* spawnedLockonPS;

	AEnemyCharacter* ScanAndSetEnemyTarget();

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	UFUNCTION(BlueprintImplementableEvent, Category = "World")
	virtual void CPPRequest_DisplayFloatingDamage(float damage, AActor* displayActor);

	//Display UI to indicate battle is over, animations, sound, etc.
	UFUNCTION(BlueprintImplementableEvent, Category = "World")
	virtual void CPPRequest_EndBattle();

	UFUNCTION(BlueprintImplementableEvent, Category = "UI")
	void CPPRequest_TogglePartyMenu();

	UFUNCTION(BlueprintCallable, Category = "Equipment")
	void Debug_EquipWeapon();

	void ProcessWeaponTrace();
	void ProcessSwordWeaponTrace();


public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};
