// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "BaseAction.generated.h"

UENUM(BlueprintType)
enum class EActionName : uint8
{
	AN_Slash UMETA(DisplayName = "Melee_Slash"),
	AN_Lunge UMETA(DisplayName = "Melee_Lunge"),
	AN_FastAction UMETA(DisplayName = "Magic_Rychlium")
};

UCLASS()
class PROJECTCRYSTAL_API ABaseAction : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseAction();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Action")
	EActionName actionName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Action")
	UParticleSystemComponent* onActivateParticleSystem;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Action")
	UAnimMontage* montageOnActivate;

	virtual void ActivateAction(AActor* actorInstigator, AActor* actorTarget);
	
};
