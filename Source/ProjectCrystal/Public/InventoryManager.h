// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "Public/BaseEquipment.h"
#include "Public/BaseCharacter.h"
#include "InventoryManager.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTCRYSTAL_API UInventoryManager : public UObject
{
	GENERATED_BODY()
	
public:
	UInventoryManager();

	virtual class UWorld* GetWorld() const override;
	
	// Set this to a valid world right after creation
	UPROPERTY(Transient)
	UWorld * World;

	UFUNCTION(BlueprintCallable, Category="Equipment")
	void AddEquipment(TSubclassOf<ABaseEquipment> item);

	UFUNCTION(BlueprintCallable, Category = "Equipment")
	TArray<TSubclassOf<ABaseEquipment>> GetEquipmentArray();
	
	UFUNCTION(BlueprintCallable, Category = "Equipment")
	ABaseEquipment* GetEquipmentDefaults(int32 index);

	UFUNCTION(BlueprintCallable, Category = "Equipment")
	void EquipWeaponLeft(ABaseCharacter* character, ABaseEquipment* item);
	
	UFUNCTION(BlueprintCallable, Category = "Equipment")
	void EquipWeaponRight(ABaseCharacter* character, ABaseEquipment* item);

	UFUNCTION(BlueprintCallable, Category = "Equipment")
	void SpawnEquippedWeapon(ABaseCharacter* character, ABaseEquipment* item);

	//Use negative numbers to subtract amount, positive adds money
	UFUNCTION(BlueprintCallable, Category = "Items")
	void AddSubtractGold(int32 amount);
	
	UFUNCTION(BlueprintCallable, Category = "Items")
	int32 GetGoldAmount();
private:
	UPROPERTY()
	int32 goldAmount;

	//TArray<TSubclassOf<ABaseEquipment>> equipmentArray;
	TArray<TSubclassOf<ABaseEquipment>> equipmentArray;
	
	
};
