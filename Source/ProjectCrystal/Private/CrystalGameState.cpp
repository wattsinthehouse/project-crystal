// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectCrystal.h"
#include "Public/CrystalGameState.h"

ACrystalGameState::ACrystalGameState()
{

}

void ACrystalGameState::RemoveBattleEnemy(AEnemyCharacter* deadEnemy)
{
	//remove the dead enemy from array of battle enemies
	//add dropped item from list
	
	goldFromBattle += deadEnemy->goldDropAmount;
	deadEnemy->CleanUpOnDeath();
}

void ACrystalGameState::CheckEndBattleStatus()
{
	//todo: check if battle is over then call below

	if (!player) return;

	player->EndBattle();

}
void ACrystalGameState::ResetBattleVariables()
{
	goldFromBattle = 0;
}

//Save and load game
void ACrystalGameState::SaveGame(ABaseCharacter* playerCharacter, int32 saveSlotIndex)
{
	UCrystalSaveGame* saveGameInstance = Cast<UCrystalSaveGame>(UGameplayStatics::CreateSaveGameObject(UCrystalSaveGame::StaticClass()));
	saveGameInstance->saveSlotIndex = saveSlotIndex;
	saveGameInstance->saveSlotName = "SaveSlot" + FString::FromInt(saveSlotIndex);

	saveGameInstance->mapFileName = mapFileName;
	saveGameInstance->mapDisplayName = mapDisplayName;

	//SaveGameInstance->PlayerName = MyPlayerName;
	UGameplayStatics::SaveGameToSlot(saveGameInstance, saveGameInstance->saveSlotName, saveGameInstance->saveSlotIndex);
}
UCrystalSaveGame* ACrystalGameState::GetSaveGameData(int32 saveSlotIndex)
{
	UCrystalSaveGame* loadGameInstance = Cast<UCrystalSaveGame>(UGameplayStatics::CreateSaveGameObject(UCrystalSaveGame::StaticClass()));
	loadGameInstance = Cast<UCrystalSaveGame>(UGameplayStatics::LoadGameFromSlot("SaveSlot" + FString::FromInt(saveSlotIndex), saveSlotIndex));

	return loadGameInstance;
}
void ACrystalGameState::LoadSaveGame(int32 saveSlotIndex)
{
	//pass in game save slot instead to actually set the variables since we have game slot already?
}
void ACrystalGameState::DeleteSaveGame(int32 saveSlotIndex)
{
	UGameplayStatics::DeleteGameInSlot("SaveSlot" + FString::FromInt(saveSlotIndex), saveSlotIndex);
}

void ACrystalGameState::SpawnActivePartyMembers()
{

}
void ACrystalGameState::AddActivePartyMember(APlayerCharacter* playerToAdd)
{
	UWorld* world = GetWorld();
	if (!world)
	{
		//add ulog
		return;
	}
}
void ACrystalGameState::RemoveActivePartyMember(APlayerCharacter* playerToRemove)
{

}
void ACrystalGameState::AddNonActivePartyMember(APlayerCharacter* playerToAdd)
{

}
void ACrystalGameState::RemoveNonActivePartyMember(APlayerCharacter* playerToRemove)
{

}