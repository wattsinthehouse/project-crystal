// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectCrystal.h"
#include "Animation/AnimMontage.h"
#include "Public/MeleeAction.h"


void AMeleeAction::ActivateAction(AActor* actorInstigator, AActor* actorTarget)
{
	if (actionName == EActionName::AN_Lunge)
	{
		ActiveLungeAction(actorInstigator, actorTarget);
	}

}

void AMeleeAction::ActiveLungeAction(AActor* actorInstigator, AActor* actorTarget)
{
	ACharacter* character = Cast<ACharacter>(actorInstigator);
	if (!character)
		return;

	if (montageOnActivate != nullptr)
	{
		//UAnimMontage* MyMontage = player->equippedActions[0].GetDefaultObject()->actionAnimMontage;
		if (montageOnActivate->IsValidLowLevel())
		{
			USkeletalMeshComponent *Mesh = character->GetMesh();
			if (Mesh)
			{
				UAnimInstance *AnimInst = Mesh->GetAnimInstance();
				AnimInst->RootMotionMode = ERootMotionMode::RootMotionFromEverything;
				if (AnimInst)
				{
					AnimInst->Montage_Play(montageOnActivate);
				}
			}
		}
	}

	//FVector launchForce = FVector(20000, 0, 0);
	////character->GetCharacterMovement()->AddImpulse(launchForce);
	//character->Mesh->AddForce(launchForce, NAME_None, true);
	//character->AddActorLocalOffset(launchForce, false, nullptr, ETeleportType::None);
	//get movement component and call function "getforwardvector".  Basically model similar how moving the character is.
	//See: https://answers.unrealengine.com/storage/temp/17424-knockpawnback.jpg
	if (actorInstigator != nullptr || actorTarget != nullptr)
	{

	}

}