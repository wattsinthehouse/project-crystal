// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectCrystal.h"
#include "Public/AI_WaypointActor.h"


// Sets default values
AAI_WaypointActor::AAI_WaypointActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AAI_WaypointActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAI_WaypointActor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

