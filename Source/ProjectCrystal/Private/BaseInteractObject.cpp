// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectCrystal.h"
#include "Public/BaseCharacter.h"
#include "Public/BaseInteractObject.h"


// Sets default values
ABaseInteractObject::ABaseInteractObject(const FObjectInitializer& ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	/*overlaySphereComponent = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("Highlight overlay"));
	overlaySphereComponent->SetSphereRadius(250.0f);
	overlaySphereComponent->AttachParent = RootComponent;
	overlaySphereComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	overlaySphereComponent->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Overlap);

	overlaySphereComponent->OnComponentEndOverlap.AddDynamic(this, &ABaseInteractObject::OnWorldOverlapEnd);
	overlaySphereComponent->OnComponentBeginOverlap.AddDynamic(this, &ABaseInteractObject::OnWorldOverlapBegin);*/

}

// Called when the game starts or when spawned
void ABaseInteractObject::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABaseInteractObject::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ABaseInteractObject::StartInteract()
{
	//Create timer that will allow the player some variable time to use this object here

	//After timer ends, call fire interact event
	FireInteractEvent();
}
void ABaseInteractObject::FireInteractEvent()
{

}

void ABaseInteractObject::StopInteract()
{
	
}


void ABaseInteractObject::OnWorldOverlapBegin(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("OnWorldOverlapBegin called"));

	instigatorActor = OtherActor;
	ABaseCharacter* player = Cast<ABaseCharacter>(OtherActor);
	if (!player)
		return;

	player->interactObject = this;

	/*ABaseWorldCharacter* player = Cast<ABaseWorldCharacter>(OtherActor);
	if (player != nullptr)
	{
		instigatorActor = OtherActor;
		player->interactObject = this;
		ABasePlayerController* pc = Cast<ABasePlayerController>(player->GetController());
		if (pc)
		{
			AWorldHUD* hud = Cast<AWorldHUD>(pc->GetHUD());
			if (hud)
			{
				hud->SetInteractTexture(interactTextureHUD);
			}
		}
	}*/
}
void ABaseInteractObject::OnWorldOverlapEnd(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		UE_LOG(LogTemp, Warning, TEXT("OnWorldOverlapEnd called"));

		instigatorActor = nullptr;
		ABaseCharacter* player = Cast<ABaseCharacter>(OtherActor);
		if (!player)
			return;

		player->interactObject = nullptr;
		/*ABaseWorldCharacter* player = Cast<ABaseWorldCharacter>(OtherActor);
		if (player != nullptr)
		{
			instigatorActor = nullptr;
			player->interactObject = nullptr;
			ABasePlayerController* pc = Cast<ABasePlayerController>(player->GetController());
			if (pc)
			{
				AWorldHUD* hud = Cast<AWorldHUD>(pc->GetHUD());
				if (hud)
				{
					hud->ResetInterractUI();
				}
			}
		}*/
	}
}