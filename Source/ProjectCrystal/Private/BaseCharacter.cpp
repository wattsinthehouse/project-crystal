// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectCrystal.h"
#include "Public/BaseCharacter.h"


// Sets default values
ABaseCharacter::ABaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bInBattle = false;
}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	bIsDead = false;
	charStats.currentHealth = charStats.maxHealth;
}

// Called every frame
void ABaseCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

float ABaseCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	//Display damage on hud

	const float damageAmount = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	charStats.currentHealth -= damageAmount;
	if (charStats.currentHealth <= 0.0f)
		StartDeathProcess();

	return damageAmount;
}
void ABaseCharacter::StartDeathProcess()
{
	bIsDead = true;
	UE_LOG(LogTemp, Warning, TEXT("Enemy Died"));

	CPPRequest_OnDeath();
	//play death animation
	//at end of death animation, dissolve enemy
	//destory actor
}

void ABaseCharacter::SpawnWeapon(UClass* weapon)
{
	FActorSpawnParameters params = FActorSpawnParameters();
	ABaseEquipment* temp = GetWorld()->SpawnActor<ABaseEquipment>(weapon, FVector(0, 0, 0), FRotator(0, 0, 0), params);
	
	if (temp == nullptr)
		return;

	if (temp->IsValidRootComponent())
		temp->AttachRootComponentTo(GetMesh(), "ThighWeaponSocket_L", EAttachLocation::SnapToTarget, false);

	equippedWeapon = temp;

	/*FActorSpawnParameters params = FActorSpawnParameters();
	inventoryManager->GetEquipmentArray()[0]->GetDefaultObject();

	ABaseEquipment* temp = GetWorld()->SpawnActor<ABaseEquipment>(inventoryManager->GetEquipmentArray()[0]->GetDefaultObject()->GetClass(),
		FVector(0, 0, 0), FRotator(0, 0, 0), params);


	if (temp == nullptr)
		return;


	if (temp->IsValidRootComponent())
		temp->AttachRootComponentTo(GetMesh(), "ThighWeaponSocket_L", EAttachLocation::SnapToTarget, false);


	equippedWeapon = temp;*/
}
void ABaseCharacter::CleanUpOnDeath()
{
	equippedWeapon->Destroy();
	equippedWeapon = nullptr;
}