// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectCrystal.h"
#include "Public/PlayerHUD.h"
#include "Public/PlayerCharacter.h"
#include "Public/CrystalAIController.h"
#include "Public/EnemyCharacter.h"


AEnemyCharacter::AEnemyCharacter(const FObjectInitializer& ObjectInitializer)
{
	battleSphereComponent = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("OverlaySphereComponent"));
	battleSphereComponent->AttachParent = RootComponent;
	battleSphereComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	battleSphereComponent->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Overlap);

	battleSphereComponent->OnComponentEndOverlap.AddDynamic(this, &AEnemyCharacter::OnWorldOverlapEnd);
	battleSphereComponent->OnComponentBeginOverlap.AddDynamic(this, &AEnemyCharacter::OnWorldOverlapBegin);

	patrolBoxComponent = ObjectInitializer.CreateDefaultSubobject<UBoxComponent>(this, TEXT("BoxSphereComponent"));
	patrolBoxComponent->AttachParent = RootComponent;
	patrolBoxComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	patrolBoxComponent->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Overlap);

	patrolBoxComponent->OnComponentEndOverlap.AddDynamic(this, &AEnemyCharacter::OnPatrolBoxOverlapEnd);
	patrolBoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AEnemyCharacter::OnPatrolBoxOverlapBegin);
}
void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	TSubclassOf<ABaseEquipment> f = weaponToSpawn;

	if (weaponToSpawn)
		SpawnWeapon(weaponToSpawn->GetDefaultObject()->GetClass());
}

void AEnemyCharacter::OnWorldOverlapBegin(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	//UE_LOG(LogTemp, Warning, TEXT("OnWorldOverlapBegin called"));

	APlayerCharacter* character = Cast<APlayerCharacter>(OtherActor);
	if (!character)
		return;

	APlayerController* pc = Cast<APlayerController>(character->Controller);
	if (!pc)
		return;

	APlayerHUD* hud = Cast<APlayerHUD>(pc->GetHUD());
	if (!hud)
		return;

	bInBattle = true;
	character->StartBattle();
	hud->CPPRequest_DrawStartBattleAnimation();
}
void AEnemyCharacter::OnWorldOverlapEnd(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	//UE_LOG(LogTemp, Warning, TEXT("OnWorldOverlapEnd called"));
}
void AEnemyCharacter::OnPatrolBoxOverlapBegin(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	//If the colliding actor is of type enemy character then ignore
	AEnemyCharacter* character = Cast<AEnemyCharacter>(OtherActor);
	if (character)
		return;

	ACrystalAIController* controller = Cast<ACrystalAIController>(GetController());
	if (!controller)
		return;

	controller->actorsInPatrolArea.Add(OtherActor);
	
}
void AEnemyCharacter::OnPatrolBoxOverlapEnd(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ACrystalAIController* controller = Cast<ACrystalAIController>(GetController());
	if (!controller)
		return;

	controller->actorsInPatrolArea.Remove(OtherActor);
}

float AEnemyCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	const float damageAmount = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	UE_LOG(LogTemp, Warning, TEXT("Enemy hurt: %f"), damageAmount);

	return damageAmount;
}