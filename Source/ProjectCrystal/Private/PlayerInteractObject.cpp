// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectCrystal.h"
#include "Animation/AnimMontage.h"
#include "Public/PlayerInteractObject.h"

APlayerInteractObject::APlayerInteractObject(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	mesh = CreateOptionalDefaultSubobject<USkeletalMeshComponent>("Mesh");
	if (mesh)
	{
		mesh->AlwaysLoadOnClient = true;
		mesh->AlwaysLoadOnServer = true;
		mesh->bOwnerNoSee = false;
		mesh->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::AlwaysTickPose;
		mesh->bCastDynamicShadow = true;
		mesh->bAffectDynamicIndirectLighting = true;
		mesh->PrimaryComponentTick.TickGroup = TG_PrePhysics;
		mesh->bChartDistanceFactor = true;
		//Mesh->AttachParent = CapsuleComponent;
		//static FName MeshCollisionProfileName(TEXT("CharacterMesh"));
		//mesh->SetCollisionProfileName(MeshCollisionProfileName);
		mesh->bGenerateOverlapEvents = false;
		mesh->bCanEverAffectNavigation = false;
	}

	RootComponent = mesh;

	overlaySphereComponent = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("Highlight overlay"));
	overlaySphereComponent->SetSphereRadius(250.0f);
	overlaySphereComponent->AttachParent = RootComponent;
	overlaySphereComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	overlaySphereComponent->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Overlap);

	overlaySphereComponent->OnComponentEndOverlap.AddDynamic(this, &ABaseInteractObject::OnWorldOverlapEnd);
	overlaySphereComponent->OnComponentBeginOverlap.AddDynamic(this, &ABaseInteractObject::OnWorldOverlapBegin);
}

void APlayerInteractObject::StartInteract()
{
	Super::StartInteract();
}

void APlayerInteractObject::FireInteractEvent()
{

	if (!mesh || !montageOnInteract || !bInteractable)
		return;

	if (montageOnInteract->IsValidLowLevel())
	{
		UAnimInstance *AnimInst = mesh->GetAnimInstance();
		AnimInst->RootMotionMode = ERootMotionMode::RootMotionFromEverything;
		if (AnimInst)
		{
			AnimInst->Montage_Play(montageOnInteract);
		}
	}

	//todo: make interact enum
	//Check what type it is, if lever or one time use, disable it here
	//if can be used multiple times (save point), then don't disable
	//For now, it will be disable after one time use.

	bInteractable = false;
	
	StopInteract();
	CPPRequest_CompleteInteract();
}
void APlayerInteractObject::StopInteract()
{
	Super::StopInteract();
}


void APlayerInteractObject::OnWorldOverlapBegin(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	//UE_LOG(LogTemp, Warning, TEXT("OnWorldOverlapBegin called"));
	if (bInteractable)
		CPPRequest_OnOverlapBegin();

	Super::OnWorldOverlapBegin(OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

}
void APlayerInteractObject::OnWorldOverlapEnd(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	/*if (OtherActor && (OtherActor != this) && OtherComp)
	{
		UE_LOG(LogTemp, Warning, TEXT("OnWorldOverlapEnd called"));

	}*/
	Super::OnWorldOverlapEnd(OtherActor, OtherComp, OtherBodyIndex);

	if (bInteractable)
		CPPRequest_OnOverlapEnd();
}

