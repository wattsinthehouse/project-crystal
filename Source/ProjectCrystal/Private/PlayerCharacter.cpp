// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectCrystal.h"
#include "Public/CrystalPlayerController.h"
#include "Public/CrystalGameState.h"
#include "DrawDebugHelpers.h"
#include "Public/PlayerCharacter.h"

APlayerCharacter::APlayerCharacter()
{
	

	//todo: do the below to attach the weapon to the player
	/*USkeletalMeshComponent* smc;
	smc->AttachTo();
	
	//https://docs.unrealengine.com/latest/INT/Engine/Content/Types/SkeletalMeshes/Sockets/index.html
	*/
	
}
void APlayerCharacter::BeginPlay()
{
	inventoryManager = NewObject<UInventoryManager>();
	inventoryManager->World = GetWorld();

	ACrystalGameState* const gameState = GetWorld() != NULL ? GetWorld()->GetGameState<ACrystalGameState>() : NULL;
	if (gameState)
		gameState->player = this;
}
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ProcessWeaponTrace();
}

void APlayerCharacter::ProcessWeaponTrace()
{
	if (!activatedAction || !equippedWeapon) return;

	if (equippedWeapon->itemStats.itemType == EItemType::IT_Sword)
		ProcessSwordWeaponTrace();
}
void APlayerCharacter::ProcessSwordWeaponTrace()
{
	UStaticMeshComponent* mesh = equippedWeapon->Mesh;
	FVector start = mesh->GetSocketLocation("Sword_Start");
	FVector end = mesh->GetSocketLocation("Sword_End");
	FHitResult outResults;
	FCollisionQueryParams queryParams;
	FCollisionObjectQueryParams objectParams;

	GetWorld()->LineTraceSingle(outResults, start, end, queryParams, objectParams);
	DrawDebugLine(GetWorld(), start, end, FColor::Red, false, 0.5f, 0, 2.f);
	
	FDamageEvent damage;

	if (outResults.GetActor() == nullptr || actorsBeingHit.Find(outResults.GetActor()) == false) return;
	AEnemyCharacter* temp = Cast<AEnemyCharacter>(outResults.GetActor());
	if (!temp) return;
	
	actorsBeingHit.Add(outResults.GetActor());
	outResults.GetActor()->TakeDamage(10.0f, damage, GetController(), this);
	CPPRequest_DisplayFloatingDamage(10.0f, outResults.GetActor());
}

void APlayerCharacter::StartBattle()
{
	//draw weapon/set fight stance
	bInBattle = true;
 
}
void APlayerCharacter::EndBattle()
{
	ACrystalGameState* gameState = GetWorld()->GetGameState<ACrystalGameState>();
	if (gameState)
	{
		inventoryManager->AddSubtractGold(gameState->goldFromBattle);
		
		CPPRequest_EndBattle();

		gameState->ResetBattleVariables();

		bInBattle = false;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ACrystalGameState is null in EndBattle function in PlayerCharacter.cpp"));
	}
}

AEnemyCharacter* APlayerCharacter::ScanAndSetEnemyTarget()
{
	AEnemyCharacter* enemyTarget = nullptr;

	ACrystalPlayerController* pc = Cast<ACrystalPlayerController>(GetController());
	if (!pc)
		return enemyTarget;
	
	FCollisionResponseParams ResponseParam(ECollisionResponse::ECR_Overlap);
	FCollisionQueryParams LineParams(FName(TEXT("ComponentIsVisibleFrom")), true);
	FVector PlayerPosition = pc->GetFocalLocation();
	FVector end = PlayerPosition + pc->GetControlRotation().Vector() * 1024;

	TArray<FHitResult> HitResults;

	GetWorld()->LineTraceMultiByChannel(HitResults, PlayerPosition, end, ECC_WorldDynamic, LineParams, ResponseParam);
	if (HitResults.Num() > 0)
	{
		TArray<FHitResult>::TConstIterator ActorIt = HitResults.CreateConstIterator();
		for (; ActorIt; ++ActorIt)
		{
			DrawDebugLine(GetWorld(), PlayerPosition, end, FColor::Green, false, 0.5f, 0, 5.f);
			AActor *Actor = ActorIt->GetActor();
			AEnemyCharacter* enemy = Cast<AEnemyCharacter>(Actor);
			if (enemy)
			{
				//todo: get closest distance between enemyTarget and selected enemy.  If closest, then target that one. if enemy is null then set this one.
				enemyTarget = enemy;

				//todo: enhance the targeting to button to cycle enemies
				UE_LOG(LogTemp, Warning, TEXT("ActorHit"));
			}
		}

	}
	return enemyTarget;
}

void APlayerCharacter::Debug_EquipWeapon()
{
	FActorSpawnParameters params = FActorSpawnParameters();
	inventoryManager->GetEquipmentArray()[0]->GetDefaultObject();

	ABaseEquipment* temp = GetWorld()->SpawnActor<ABaseEquipment>(inventoryManager->GetEquipmentArray()[0]->GetDefaultObject()->GetClass(),
		FVector(0, 0, 0), FRotator(0, 0, 0), params);

	
	if (temp == nullptr)
		return;

	
	if (temp->IsValidRootComponent())
		temp->AttachRootComponentTo(GetMesh(), "ThighWeaponSocket_L", EAttachLocation::SnapToTarget, false);

	
	equippedWeapon = temp;
	
}