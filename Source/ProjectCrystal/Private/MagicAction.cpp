// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectCrystal.h"
#include "Public/BaseCharacter.h"
#include "Public/MagicAction.h"




void AMagicAction::ActivateAction(AActor* actorInstigator, AActor* actorTarget)
{
	if (actionName == EActionName::AN_FastAction)
	{
		ActivateFastAction(actorTarget);
	}
}

void AMagicAction::ActivateFastAction(AActor* actorTarget)
{
	ABaseCharacter* target = Cast<ABaseCharacter>(actorTarget);
	
	if (!target)
		return;

	target->GetCharacterMovement()->MaxWalkSpeed = fastMaxWalkSpeed;
	
	if (onActivateParticleSystem)
		onActivateParticleSystem->BeginPlay();
	
	//target->CharacterMovement->MaxAcceleration = 0.0f;
	
	//todo: set timer to default
	//todo: set max walkspeed  editable in ue4 (inherit from character class)
	//todo: set increased walking speed by the spell
}