// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectCrystal.h"
#include "Public/InventoryManager.h"

UInventoryManager::UInventoryManager()
{

}

UWorld* UInventoryManager::GetWorld() const
{
	return World;
}

void UInventoryManager::AddEquipment(TSubclassOf<ABaseEquipment> item)
{
	equipmentArray.Add(item);
}

TArray<TSubclassOf<ABaseEquipment>> UInventoryManager::GetEquipmentArray()
{
	return equipmentArray;
}

ABaseEquipment* UInventoryManager::GetEquipmentDefaults(int32 index)
{
	if (equipmentArray.Num() == 0 || !equipmentArray[index]) return nullptr;

	return equipmentArray[0].GetDefaultObject();
}


void UInventoryManager::EquipWeaponLeft(ABaseCharacter* character, ABaseEquipment* item)
{
	if (!character || !item) return;

	character->equippedWeapon = item;
}

void UInventoryManager::EquipWeaponRight(ABaseCharacter* character, ABaseEquipment* item)
{

}

void UInventoryManager::SpawnEquippedWeapon(ABaseCharacter* character, ABaseEquipment* item)
{
	FActorSpawnParameters params = FActorSpawnParameters();

	if (!World) return; 
	
	ABaseEquipment* temp = World->SpawnActor<ABaseEquipment>(character->equippedWeapon->GetClass(), FVector(0, 0, 0), FRotator(0, 0, 0), params);
	//ABaseEquipment* temp = World->SpawnActor<ABaseEquipment>(GetEquipmentArray()[0]->GetDefaultObject()->GetClass(), FVector(0, 0, 0), FRotator(0, 0, 0), params);

	if (temp == nullptr || !temp->IsValidRootComponent()) return;

	character->equippedWeapon = temp;
	temp->AttachRootComponentTo(character->GetMesh(), "ThighWeaponSocket_L", EAttachLocation::SnapToTarget, false);
}

void UInventoryManager::AddSubtractGold(int32 amount)
{
	goldAmount += amount;
}
int32 UInventoryManager::GetGoldAmount()
{
	return goldAmount;
}