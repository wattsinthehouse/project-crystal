// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectCrystal.h"
#include "Public/EnemyCharacter.h"
#include "Public/CrystalAIController.h"

void ACrystalAIController::BeginPlay()
{
	Super::BeginPlay();

	AEnemyCharacter* owner = Cast<AEnemyCharacter>(GetOwner());
	if (!owner || !owner->behaviorTree)
	{
		UE_LOG(LogTemp, Warning, TEXT("ACrystalAIController owner or behavior tree is null in BeginPlay in CrystalAIController.cpp"));
		return;
	}
	
	RunBehaviorTree(owner->behaviorTree);
}


void ACrystalAIController::Possess(APawn* InPawn)
{
	Super::Possess(InPawn);

	AEnemyCharacter* owner = Cast<AEnemyCharacter>(InPawn);
	if (!owner || !owner->behaviorTree)
	{
		UE_LOG(LogTemp, Warning, TEXT("ACrystalAIController owner or behavior tree is null in Possess in CrystalAIController.cpp"));
		return;
	}
	
	//RunBehaviorTree(owner->behaviorTree);
}

AActor* ACrystalAIController::FindPlayer()
{
	AActor* playerFound = nullptr;

	if (actorsInPatrolArea.Num() == 0)
		return playerFound;

	for (int i = 0; i < actorsInPatrolArea.Num(); i++)
	{
		//AActor* player = actorsInPatrolArea[i];
		//AActor* aiActor = GetOwner();
		//if (!player && !aiActor)
		//	return playerFound;

		//FVector location = player->GetActorLocation() - aiActor->GetActorLocation();
		//location.Normalize();
		////Dot product between the two (cosin between angles)
		//float dotProduct = location.DotProduct(location, aiActor->GetActorForwardVector());
		//float degrees = acos(dotProduct); //find angle to player in degrees

		//if (degrees < 0)
		//{
		//	playerFound = player;
		//	break;
		//}
		
			
		bool actorFound = LineOfSightTo(actorsInPatrolArea[i], FVector(0, 0, 0), false);

		if (actorFound)
		{
			//exit loop, return actor or bool? which one
			playerFound = actorsInPatrolArea[i];
			break;
		}
	}

	return playerFound;
}
void ACrystalAIController::PlayerLost()
{

}