// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectCrystal.h"
#include "Public/BaseCharacter.h"
#include "Public/PlayerCharacter.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Public/CrystalPlayerController.h"


ACrystalPlayerController::ACrystalPlayerController()
{
	bHasTarget = false;
}
// Begin PlayerController interface
void ACrystalPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	//This function checks if enemyTarget is set, and if it is, focus on that enemy
	CameraFollowLockOnTarget();
}
void ACrystalPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	//Axis Mappings
	InputComponent->BindAxis("MoveForward", this, &ACrystalPlayerController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ACrystalPlayerController::MoveRight);

	InputComponent->BindAxis("Turn", this, &APlayerController::AddYawInput);
	InputComponent->BindAxis("LookUp", this, &APlayerController::AddPitchInput);

	//Action Mappings
	InputComponent->BindAction("ActionOne", IE_Pressed, this, &ACrystalPlayerController::OnActionOne);
	InputComponent->BindAction("ActionOne", IE_Released, this, &ACrystalPlayerController::OnActionOneRelease);
	InputComponent->BindAction("ActionTwo", IE_Pressed, this, &ACrystalPlayerController::OnActionTwo);
	InputComponent->BindAction("ActionTwo", IE_Released, this, &ACrystalPlayerController::OnActionTwoRelease);
	InputComponent->BindAction("ActionThree", IE_Pressed, this, &ACrystalPlayerController::OnActionThree);
	InputComponent->BindAction("ActionThree", IE_Released, this, &ACrystalPlayerController::OnActionThreeRelease);
	InputComponent->BindAction("ActionFour", IE_Pressed, this, &ACrystalPlayerController::OnActionFour);
	InputComponent->BindAction("ActionFour", IE_Released, this, &ACrystalPlayerController::OnActionFourRelease);
	InputComponent->BindAction("ActionInteract", IE_Pressed, this, &ACrystalPlayerController::OnActionInteract);
	InputComponent->BindAction("ActionInteract", IE_Released, this, &ACrystalPlayerController::OnActionInteractRelease);
	InputComponent->BindAction("ActionPartyMenu", IE_Pressed, this, &ACrystalPlayerController::OnActionOpenPartyMenu);
	InputComponent->BindAction("ActionPartyMenu", IE_Released, this, &ACrystalPlayerController::OnActionOpenPartyMenuRelease);

}
// End PlayerController interface

void ACrystalPlayerController::MoveForward(float value)
{
	if (value == 0.0f)
		return;

	// find out which way is forward
	const FRotator rotation = GetControlRotation();
	const FRotator yawRotation(0, rotation.Yaw, 0);

	// get forward vector
	const FVector direction = FRotationMatrix(yawRotation).GetUnitAxis(EAxis::X);
	MoveCharacter(direction, value);
}
void ACrystalPlayerController::MoveRight(float value)
{
	if (value == 0.0f)
		return;

	// find out which way is right
	const FRotator rotation = GetControlRotation();
	const FRotator yawRotation(0, rotation.Yaw, 0);

	// get right vector
	const FVector direction = FRotationMatrix(yawRotation).GetUnitAxis(EAxis::Y);
	MoveCharacter(direction, value);
}
void ACrystalPlayerController::MoveCharacter(const FVector direction, float value)
{
	ABaseCharacter* player = Cast<ABaseCharacter>(GetCharacter());
	if (player)
		player->AddMovementInput(direction, value);
}

void ACrystalPlayerController::OnActionOne()
{
	//UE_LOG(LogTemp, Warning, TEXT("OnAttackOne called"));

	//Exit function call if player is null or if an action is not equipped.
	ABaseCharacter* player = Cast<ABaseCharacter>(GetCharacter());
	if (!player || player->equippedActions[0] == nullptr) return;
	
	if (player->bInBattle)
	{
		player->actorsBeingHit.Empty();
		player->equippedActions[0].GetDefaultObject()->ActivateAction(player, player);
		player->activatedAction = player->equippedActions[0].GetDefaultObject();
	}
	else
	{
		//perform non battle action

		//Create a nonBattleActionOne and a CPPRequest_NonBattleActionOne.  
		//Call both here.  Can use CPPRequest_NonBattleActionOne to interact with menu, remove menu, etc
		//Can use NonBattleActionOne to start conversation, interact with objects, etc.
	}
}
void ACrystalPlayerController::OnActionOneRelease()
{
	UE_LOG(LogTemp, Warning, TEXT("OnActionOneRelease called"));

	ClearActivatedAction();
}
void ACrystalPlayerController::OnActionTwo()
{
	ABaseCharacter* player = Cast<ABaseCharacter>(GetCharacter());
	if (player)
	{
		if (player->equippedActions[1] != nullptr)
			player->equippedActions[1].GetDefaultObject()->ActivateAction(player, player);
	}
}
void ACrystalPlayerController::OnActionTwoRelease()
{

}
void ACrystalPlayerController::OnActionThree()
{
	ABaseCharacter* player = Cast<ABaseCharacter>(GetCharacter());
	if (player)
	{
		if (player->equippedActions[2] != nullptr)
			player->equippedActions[2].GetDefaultObject()->ActivateAction(player, player);
	}
}
void ACrystalPlayerController::OnActionThreeRelease()
{

}
void ACrystalPlayerController::OnActionFour()
{
	//todo: create parent function that all action inputs go in to determine what function to call to allow remapping of buttons for player
	ActionLockOnProcess();
	
}
void ACrystalPlayerController::OnActionFourRelease()
{

}
void ACrystalPlayerController::OnActionInteract()
{
	ABaseCharacter* player = Cast<ABaseCharacter>(GetCharacter());
	if (!player || !player->interactObject || !player->interactObject->bInteractable) return;

	player->interactObject->StartInteract();

	USkeletalMeshComponent* mesh = player->GetMesh();
	if (!mesh || !player->interactObjectPullMontage)
		return;

	if (player->interactObjectPullMontage->IsValidLowLevel())
	{
		UAnimInstance *AnimInst = mesh->GetAnimInstance();
		AnimInst->RootMotionMode = ERootMotionMode::RootMotionFromEverything;
		if (AnimInst)
		{
			AnimInst->Montage_Play(player->interactObjectPullMontage);
		}
	}

	player->CPPRequest_StartInteractProcess();
}
void ACrystalPlayerController::OnActionInteractRelease()
{
	/*ABaseCharacter* player = Cast<ABaseCharacter>(GetCharacter());
	if (!player || !player->interactObject) return;

	player->interactObject->StopInteract();
	player->CPPRequest_StopInteractProcess();*/
}
void ACrystalPlayerController::OnActionOpenPartyMenu()
{
	APlayerCharacter* player = Cast<APlayerCharacter>(GetCharacter());
	if (!player) return;

	player->CPPRequest_TogglePartyMenu();

}
void ACrystalPlayerController::OnActionOpenPartyMenuRelease()
{

}

void ACrystalPlayerController::ActionLockOnProcess()
{
	if (!enemyTarget)
	{
		APlayerCharacter* player = Cast<APlayerCharacter>(GetCharacter());
		if (!player && !player->lockOnTargetParticleSystem)
			return;

		enemyTarget = player->ScanAndSetEnemyTarget();
		if (!enemyTarget)
			return;

		if (player->spawnedLockonPS)
		{
			player->spawnedLockonPS->AttachTo(enemyTarget->GetMesh(), "Lockon_Socket", EAttachLocation::KeepRelativeOffset, false);
			player->spawnedLockonPS->Activate();
		}
		else
		{
			//Create the lock on PS and assign it
			UParticleSystemComponent* spawnedPS = UGameplayStatics::SpawnEmitterAttached(player->lockOnTargetParticleSystem, enemyTarget->GetMesh(),
				"Lockon_Socket", FVector(0, 0, 0), FRotator::ZeroRotator, EAttachLocation::KeepRelativeOffset, false);

			player->spawnedLockonPS = spawnedPS;
		}
	}
	else
	{
		enemyTarget = nullptr;
		APlayerCharacter* player = Cast<APlayerCharacter>(GetCharacter());
		if (player && player->spawnedLockonPS)
		{
			player->spawnedLockonPS->DeactivateSystem();
			//todo: unfollow camera focus on locked on target.
		}
	}
	//trace from camera point of view. 
	//For the trace, look into doing a box trace or a multi line trace.
	//Something that can hit a range of an area.
	//if enemy found, set target to enemyTarget variable
}
void ACrystalPlayerController::CameraFollowLockOnTarget()
{
	if (!enemyTarget)
		return;

	APlayerCharacter* player = Cast<APlayerCharacter>(GetCharacter());
	if (!player)
		return;
	//player->FollowCamera->;
	//PlayerCameraManager->SetViewTarget(enemyTarget, FViewTargetTransitionParams());
	//Player
	////todo: follow/focus on emey
	//RotationInput.Yaw += 1;
	
	//player->FollowCamera->GetComponentRotation();
	
	//FMath::Lerp();
	//player->CameraBoom->RelativeLocation()
	//FRotator lockOnRotation = (enemyTarget->GetActorLocation() - player->GetActorLocation()).Rotation();
	//player->FollowCamera->SetRelativeRotation(lockOnRotation);
	

	FRotator lockOnRotation = UKismetMathLibrary::FindLookAtRotation(player->GetActorLocation(), enemyTarget->GetActorLocation());
	FRotator finalRotation = FRotator(0.0f, lockOnRotation.Yaw, 0.0f);
	player->SetActorRotation(finalRotation);
	
	FRotator temp = FRotator(-20.0f, player->GetMesh()->GetComponentRotation().Yaw + 90.0f, 0.0f);
	FRotator final = FMath::RInterpTo(GetControlRotation(), temp, 1.0f, 1.0f);
	
	SetControlRotation(final);
	// finalRotation = FMath::Lerp(player->FollowCamera->GetComponentRotation(), lockOnRotation, 1); //UKismetMathLibrary::RLerp(player->FollowCamera->GetComponentRotation(), lockOnRotation, 1, false);
	//player->FollowCamera->SetRelativeRotation(lockOnRotation);
	
	
	//player->FollowCamera->SetWorldRotation();
	
}
void ACrystalPlayerController::ClearActivatedAction()
{
	ABaseCharacter* player = Cast<ABaseCharacter>(GetCharacter());
	if (!player) return;

	player->activatedAction = nullptr;

}

void ACrystalPlayerController::TurnAtRate(float Rate)
{
	AddYawInput(Rate * 10 * GetWorld()->GetDeltaSeconds());
	// calculate delta for this frame from the rate information
}
void ACrystalPlayerController::LookUpAtRate(float Rate)
{
	AddPitchInput(Rate * 10 * GetWorld()->GetDeltaSeconds());
	// calculate delta for this frame from the rate information
}

void ACrystalPlayerController::ScanAndSetEnemyTarget()
{
	FCollisionResponseParams ResponseParam(ECollisionResponse::ECR_Overlap);
	FCollisionQueryParams LineParams(FName(TEXT("ComponentIsVisibleFrom")), true);
	FVector PlayerPosition = GetFocalLocation();
	FVector end = PlayerPosition + GetControlRotation().Vector() * 1024;

	TArray<FHitResult> HitResults;

	GetWorld()->LineTraceMultiByChannel(HitResults, PlayerPosition, end, ECC_WorldDynamic, LineParams, ResponseParam);
	if (HitResults.Num() > 0)
	{
		TArray<FHitResult>::TConstIterator ActorIt = HitResults.CreateConstIterator();
		for (; ActorIt; ++ActorIt)
		{
			DrawDebugLine(GetWorld(), PlayerPosition, end, FColor::Green, false, 0.5f, 0, 5.f);
			AActor *Actor = ActorIt->GetActor();
			AEnemyCharacter* enemy = Cast<AEnemyCharacter>(Actor);
			if (enemy)
			{
				//todo: get closest distance between enemyTarget and selected enemy.  If closest, then target that one. if enemy is null then set this one.
				enemyTarget = enemy;

				//todo: enhance the targeting to button to cycle enemies
				UE_LOG(LogTemp, Warning, TEXT("ActorHit"));
				//Actor->SetActorHiddenInGame(true);
				//CurrentlyHiddenActors.Add(TWeakObjectPtr<AActor>(Actor));
			}
		}

	}
}