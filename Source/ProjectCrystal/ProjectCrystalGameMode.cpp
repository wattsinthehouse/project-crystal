// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "ProjectCrystal.h"
#include "ProjectCrystalGameMode.h"
#include "Public/CrystalPlayerController.h"
#include "Public/PlayerHUD.h"
#include "Public/CrystalGameState.h"
#include "ProjectCrystalCharacter.h"

AProjectCrystalGameMode::AProjectCrystalGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Characters/Player/Player_General"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
		PlayerControllerClass = ACrystalPlayerController::StaticClass();
		HUDClass = APlayerHUD::StaticClass();
		GameStateClass = ACrystalGameState::StaticClass();
	}
}
