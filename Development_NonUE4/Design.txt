
Tech Design:
------------------
Action:	
	Design 1:
	Not including lock on, but these skills indicate if a target is needed
	I don't want to limit actions based off of target needed vs non tagret
	
	-BaseAction
		-NonTarget
			Melee
			spells	
			etc
		-Target
			buffs/debuffs
			Melee
			etc
		
	
	Design 2:
	This design seems to be better to allow more flexibility in the design
	
	Include bool if target is needed in BaseAction
	-BaseAction
		bTargetNeeded
		animation
		particle system
		baseDamage
		
		-Melee
			WeaponTrace
			FVector moveCharOnAttak // some melee attacks might need to move the player
		-Magic/glove
			SpawnProjectile
			Set Buff timer
			Subtract MP
		-Gun
			WeaponTrace
			//SpawnProjectile  //Will guns ever spawn the projectile?
			//Reload animation should be associated to weapon
		-Shield
	
	Design 3:
	???
	
Battle
-----------------
For animals, drop animal materials for selling.  Don't let them drop gold.

	
Gameplay Design:
-----------------
Player Stats
	currentHealth //may be part of character class
	maxHealth //may be part of character class
	currentMP
	MP
	speed
	strength
	lore(magic)
	Precision
	
Action Names
------------------
Melee

Magic skills
	Wind
		Magic_Rychlium (fast in czech and latin combined.  First czech name, second latin name)
	Fire
		Fire Shot
	Lightning
	
Shooting

Misc
	Not sure if needed?
	
Coop actions
------------------
Depending on who starts it determines what action is done.  Bind the start of the action to one button.  Once pressed, the other teammate has to be near the starter to actually do the action.
During action, the target enemy(ies) won't move but can still be vulnerable to other attacks from teammates.  Only one coop action can be compelted at one time.  May need to add a cooldown
period before it can be used again as a group for balancing.  (30 seconds for party, 60 seconds for the person who initiated it). or a bar that has to refill.
	If extra development time, design multiple coop actions for the same person but allow the player to pick which one they want.

Action ideas:

Fire/gun,
	If starts, with wind push a bullet that pierces multiple enemies in a line and explodes afterwards
	if starts with lightning

wind/sword
	if starts, with lightning, do multiple dash attacks where lightning charges the blade.
	
lighting/lighting
	if starts, with wind, wind levitates lightning and then lightning zaps all enemies in range.

Dialogue System
------------------
Need to handle dialogue with words just being said and wait for prompt from player before continuing on to players lines or npcs.
Create 3(or 4) structs to house the text.
	NPC Response
	Player Response
	Player Choices
	Dialogue Flow
Create UserwWidget that will display the npc response/player choices
	Display the text 
	
UI design
------------------
	Main Menu
	
	Party Menu
		Display player input but allow cancel button to close party menu
		Show top row of buttons for ease of navigating.  Such as  Items | Equipment | Party | Skills | Stats | Map | Options
		Item Menu
		Equipment menu
			Two panels, 
				left side shows what equipment is equipped
				right side shows equipment that the player can equip
					equipment icons are 80x80 pixels
					extra space 3 pixels, so 83x83 for grid 
					equipment grid 10 rows x 5 columns (for grid, 83 * 10 (830), 83 * 5 (415))
					Create for loop that will cycle all equipment items and create a widget for each
						PM_EquipmentIcon
							Disable OnClick if the item is currently equipped
							OnClick - Pass this equipment info to Bottom panel for display, display clickable button to equip
							OnClick - Second click, if not on the button then hide and "unfocus" the button (remove bottom equipment stats)
									- Second click, if not on the button and click is on a different equipment piece, remove the first button and act like it is the first click on the new equipment icon
									- Second click, Call character equip item and pass self.  Update left panel, right panel, and bottom panel, if equipment is weapon then spawn weapon in world
							If any equipment is equipped, show texture in one of the corners of the item to indicate it is equipped. (like a green diagonal with "E" in it.
					When previewing stats, show up/down arrows for better/worse stats and color coordinated.  Probably green/red
				How to handle navigating back to parent page?
					On cancel button, remove equipment menu from parent
			bottom panel
				Two stat sections.  Character stats and equipment stats
		Stat Menu
		Options

Controller Button Default Mappings
------------------
	In combat
	-----------------
	Square - Dodge/Block?
	X - Basic attack
	O - Dodge/Block?
	Triangle
	R3 - Lock on
	Right stick - If lock on target, used to change focus
				- If no lock on, move camera
	Left stick - move character
	L1 + button - activate skill
	
	Out of combat
	-----------------
	Square - Interact with objects
	X
	O - 
	Triangle - Party Menu
	Right stick - Move camera
	Left stick - move character
	
	Menus
	-----------------
	Square - Interact with objects
	X
	O - If in party menu, cancel current menu/close menu if on main part menu
	Triangle - Party Menu
	Right stick - Move camera
	Left stick - move character
	

Interacting/talking with npcs and cut scenes
------------------

