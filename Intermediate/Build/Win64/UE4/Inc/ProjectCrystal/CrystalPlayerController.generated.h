// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	C++ class header boilerplate exported from UnrealHeaderTool.
	This is automatically generated by the tools.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectBase.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROJECTCRYSTAL_CrystalPlayerController_generated_h
#error "CrystalPlayerController.generated.h already included, missing '#pragma once' in CrystalPlayerController.h"
#endif
#define PROJECTCRYSTAL_CrystalPlayerController_generated_h

#define ProjectCrystal_Source_ProjectCrystal_Public_CrystalPlayerController_h_15_RPC_WRAPPERS
#define ProjectCrystal_Source_ProjectCrystal_Public_CrystalPlayerController_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define ProjectCrystal_Source_ProjectCrystal_Public_CrystalPlayerController_h_15_INCLASS_NO_PURE_DECLS \
	private: \
	static void StaticRegisterNativesACrystalPlayerController(); \
	friend PROJECTCRYSTAL_API class UClass* Z_Construct_UClass_ACrystalPlayerController(); \
	public: \
	DECLARE_CLASS(ACrystalPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), 0, ProjectCrystal, NO_API) \
	DECLARE_SERIALIZER(ACrystalPlayerController) \
	/** Indicates whether the class is compiled into the engine */    enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	virtual UObject* _getUObject() const override { return const_cast<ACrystalPlayerController*>(this); }


#define ProjectCrystal_Source_ProjectCrystal_Public_CrystalPlayerController_h_15_INCLASS \
	private: \
	static void StaticRegisterNativesACrystalPlayerController(); \
	friend PROJECTCRYSTAL_API class UClass* Z_Construct_UClass_ACrystalPlayerController(); \
	public: \
	DECLARE_CLASS(ACrystalPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), 0, ProjectCrystal, NO_API) \
	DECLARE_SERIALIZER(ACrystalPlayerController) \
	/** Indicates whether the class is compiled into the engine */    enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	virtual UObject* _getUObject() const override { return const_cast<ACrystalPlayerController*>(this); }


#define ProjectCrystal_Source_ProjectCrystal_Public_CrystalPlayerController_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACrystalPlayerController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACrystalPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACrystalPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACrystalPlayerController); \
private: \
	/** Private copy-constructor, should never be used */ \
	NO_API ACrystalPlayerController(const ACrystalPlayerController& InCopy); \
public:


#define ProjectCrystal_Source_ProjectCrystal_Public_CrystalPlayerController_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private copy-constructor, should never be used */ \
	NO_API ACrystalPlayerController(const ACrystalPlayerController& InCopy); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACrystalPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACrystalPlayerController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACrystalPlayerController)


#define ProjectCrystal_Source_ProjectCrystal_Public_CrystalPlayerController_h_12_PROLOG
#define ProjectCrystal_Source_ProjectCrystal_Public_CrystalPlayerController_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectCrystal_Source_ProjectCrystal_Public_CrystalPlayerController_h_15_RPC_WRAPPERS \
	ProjectCrystal_Source_ProjectCrystal_Public_CrystalPlayerController_h_15_INCLASS \
	ProjectCrystal_Source_ProjectCrystal_Public_CrystalPlayerController_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProjectCrystal_Source_ProjectCrystal_Public_CrystalPlayerController_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectCrystal_Source_ProjectCrystal_Public_CrystalPlayerController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	ProjectCrystal_Source_ProjectCrystal_Public_CrystalPlayerController_h_15_INCLASS_NO_PURE_DECLS \
	ProjectCrystal_Source_ProjectCrystal_Public_CrystalPlayerController_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProjectCrystal_Source_ProjectCrystal_Public_CrystalPlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
